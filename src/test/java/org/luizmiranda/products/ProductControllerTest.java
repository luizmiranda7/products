package org.luizmiranda.products;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.luizmiranda.products.rest.dtos.ImageDTO;
import org.luizmiranda.products.rest.dtos.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void addProduct() {
		
		ProductDTO product = new ProductDTO();
		product.setName("p1");
		product.setDescription("d1");
		
		ImageDTO image = new ImageDTO();
		image.setType("PNG");
		product.getImages().add(image);
		
		ResponseEntity<ProductDTO> response = restTemplate.postForEntity("/api/products", product, ProductDTO.class);
		
		Assert.assertEquals(response.getStatusCodeValue(), 200);
		Assert.assertNotNull(response.getBody());
		Assert.assertNotNull(response.getBody().getId());
		
		ProductDTO savedProduct = response.getBody();
		Assert.assertFalse(savedProduct.getImages().isEmpty());

		ImageDTO savedImage = savedProduct.getImages().iterator().next();
		Assert.assertNotNull(savedImage.getId());
		
	}
	
	@Test
	public void addProductWithNoName() {
		
		ProductDTO product = new ProductDTO();
		product.setDescription("d2");
		
		ResponseEntity<ProductDTO> response = restTemplate.postForEntity("/api/products", product, ProductDTO.class);
		
		Assert.assertEquals(400, response.getStatusCodeValue());
		
	}
	
	@Test
	public void addProductWithNoDescription() {
		
		ProductDTO product = new ProductDTO();
		product.setName("p3");
		
		ResponseEntity<ProductDTO> response = restTemplate.postForEntity("/api/products", product, ProductDTO.class);
		
		Assert.assertEquals(400, response.getStatusCodeValue());
		
	}

	
	@Test
	public void addProductWithImageWithNoType() {
		
		ProductDTO product = new ProductDTO();
		product.setName("p4");
		product.setDescription("d4");
		
		product.getImages().add(new ImageDTO());
		
		ResponseEntity<ProductDTO> response = restTemplate.postForEntity("/api/products", product, ProductDTO.class);
		
		Assert.assertEquals(500, response.getStatusCodeValue());
		
	}

	
	@Test
	public void updateProduct() {
		
		ProductDTO product = new ProductDTO();
		product.setName("p5");
		product.setDescription("d5");
		
		ImageDTO image = new ImageDTO();
		image.setType("PNG");
		product.getImages().add(image);
		
		ResponseEntity<ProductDTO> createResponse = restTemplate.postForEntity("/api/products", product, ProductDTO.class);
		
		ProductDTO savedProduct = createResponse.getBody();
		savedProduct.setName("p5-changed");
		savedProduct.setDescription("d5-changed");

		ImageDTO savedImage = savedProduct.getImages().iterator().next();
		savedImage.setType("JPEG");
		
		restTemplate.put("/api/products", savedProduct);
		ResponseEntity<ProductDTO> updateResponse = restTemplate.getForEntity("/api/products/" + savedProduct.getId(), ProductDTO.class);

		Assert.assertEquals(updateResponse.getStatusCodeValue(), 200);
		
		ProductDTO updatedProduct = updateResponse.getBody();
		Assert.assertNotNull(updatedProduct);
		Assert.assertEquals(savedProduct.getId(), updatedProduct.getId());
		Assert.assertEquals(savedProduct.getName(), updatedProduct.getName());
		Assert.assertEquals(savedProduct.getDescription(), updatedProduct.getDescription());
		
		ImageDTO updatedImage = updatedProduct.getImages().iterator().next();
		Assert.assertEquals(savedImage.getType(),updatedImage.getType());
		
	}
	
	@Test
	public void deleteProduct() {
		
		ProductDTO product = new ProductDTO();
		product.setName("p6");
		product.setDescription("d6");
		
		ImageDTO image = new ImageDTO();
		image.setType("PNG");
		product.getImages().add(image);
		
		ResponseEntity<ProductDTO> createResponse = restTemplate.postForEntity("/api/products", product, ProductDTO.class);
		
		ProductDTO savedProduct = createResponse.getBody();
		
		restTemplate.delete("/api/products/" + savedProduct.getId());
		ResponseEntity<ProductDTO> updateResponse = restTemplate.getForEntity("/api/products/" + savedProduct.getId(), ProductDTO.class);

		Assert.assertEquals(404, updateResponse.getStatusCodeValue());
		
	}
	

}
