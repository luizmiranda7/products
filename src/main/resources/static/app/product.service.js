(function() {
    'use strict';

    angular
        .module('productsApp')
        .factory('Product', Product);

    Product.$inject = ['$resource'];

    function Product ($resource) {
    	var resourceUrl =  'api/products/:id';

        return $resource(resourceUrl, {}, {
            'getAll': { 
                method:'GET',
                url: 'api/products',
                isArray: true
            },
            'getAllThin': { 
                method:'GET',
                url: 'api/products/thin',
                isArray: true
            },
            'update': { method:'PUT' }
        });
    }
})();
