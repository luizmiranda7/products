(function() {
    'use strict';

    angular
        .module('productsApp')
        .controller('ProductDialogController', ProductDialogController);

    ProductDialogController.$inject = ['$scope', '$uibModal', '$uibModalInstance', 'Product', 'product', 'products'];

    function ProductDialogController ($scope, $uibModal, $uibModalInstance, Product, product, products) {
        var vm = this;
        
        vm.product = product;
        vm.products = products.filter(function(p) { return p.id != vm.product.id });

        vm.clear = clear;
        vm.save = save;
        vm.addImage = addImage;

        vm.imagesConfig = {
            getData: function () { return vm.product.images; }, 
                   
            options: { 
                showDeleteButton: true,
                showEditButton: true,
                editable: true,
                perRowEditModeEnabled: true,
                columns: [{ field: 'id', title: 'Id', inputType: 'number', disabled: true }, 
                          { field: 'type', title: 'Type', required: true}]
            }
        };
        
        function clear() {
        	$uibModalInstance.dismiss('cancel');
        }
        
        function save() {
        	vm.isSaving = true;
        	
        	vm.product.images = vm.product.images.filter(function(image) {
        		return !image.$deleted;
        	});
        	
            if (vm.product.id !== null) {
                Product.update(vm.product, onSaveSuccess, onSaveError);
            } else {
                Product.save(vm.product, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess(result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        function addImage() {
            vm.product.images.push({
                    $added: true,
                    $editable: true
                });
        }
    }
})();
