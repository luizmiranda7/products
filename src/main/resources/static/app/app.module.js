(function() {
    'use strict';

    angular
        .module('productsApp', [
            'ngStorage',
            'ngResource',
            'ngAria',
            'ui.bootstrap',
            'simpleGrid'
            ]);
})();
