(function() {
    'use strict';

    angular
        .module('productsApp')
        .controller('ProductController', ProductController);

    ProductController.$inject = ['$scope', '$uibModal', 'Product'];

    function ProductController ($scope, $uibModal, Product) {
        var vm = this;

        vm.thinProducts = Product.getAllThin();
        vm.products = Product.getAll();
        
        vm.updateProductList = updateProductList;
        vm.openProductEdition = openProductEdition;
        vm.deleteProduct = deleteProduct;
        
        function updateProductList() {
            vm.thinProducts = Product.getAllThin();
        }
        
        function openProductEdition(id) {
        	$uibModal.open({
                templateUrl: 'app/product-dialog.html',
                controller: 'ProductDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg',
                resolve: {
                    product: ['Product', function(Product) {
                    	if(id)
                    		return Product.get({id : id}).$promise;
                    	else {
                    		return {
                    			id: null,
                    			name: "",
                    			images: []
                    		};
                    	}
                    }],
                    products: function() {
                        return vm.thinProducts;
                    }
                }
            }).result.then(updateProductList);
        }
        
        function deleteProduct(id) {
        	
        	Product.delete({id: id}, vm.updateProductList);
        	
        }
    }
})();
