package org.luizmiranda.products.rest;

import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.luizmiranda.products.rest.dtos.ImageDTO;
import org.luizmiranda.products.rest.dtos.ProductDTO;
import org.luizmiranda.products.rest.dtos.ThinProductDTO;
import org.luizmiranda.products.services.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing Products.
 */
@RestController
@RequestMapping("/api")
public class ProductController {

    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    /**
     * POST /products : Create a new product.
     *
     * @param productDTO the productDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new productDTO
     */
    @PostMapping("/products")
    public ResponseEntity<ProductDTO> createProduct(@Valid @RequestBody ProductDTO productDTO) {
        log.debug("REST request to save Product : {}", productDTO);
        if (productDTO.getId() != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("error", "idNotNull");
            return ResponseEntity.badRequest().headers(headers).body(null);
        }

        ProductDTO result = null;
        try {

            result = productService.save(productDTO);

            HttpHeaders headers = new HttpHeaders();
            headers.add("product", result.getId().toString());
            return ResponseEntity.ok().headers(headers).body(result);

        } catch (Exception e) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("error", e.getMessage());
            return ResponseEntity.badRequest().headers(headers).body(null);
        }
    }

    /**
     * PUT /products : Updates an existing product.
     *
     * @param productDTO the productDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated productDTO
     */
    @PutMapping("/products")
    public ResponseEntity<ProductDTO> updateProduct(@Valid @RequestBody ProductDTO productDTO) {
        log.debug("REST request to update Product : {}", productDTO);

        if (productDTO.getId() == null) {
            return createProduct(productDTO);
        }

        ProductDTO result = null;
        try {
            result = productService.save(productDTO);

            HttpHeaders headers = new HttpHeaders();
            headers.add("product", result.getId().toString());
            return ResponseEntity.ok().headers(headers).body(result);

        } catch (Exception e) {

            HttpHeaders headers = new HttpHeaders();
            headers.add("product", e.getMessage());
            return ResponseEntity.badRequest().headers(headers).body(null);
        }
    }

    /**
     * DELETE /products/:id : delete the "id" product.
     *
     * @param id the id of the productDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        log.debug("REST request to delete Product : {}", id);
        productService.delete(id);

        HttpHeaders headers = new HttpHeaders();
        headers.add("product", id.toString());
        return ResponseEntity.ok().headers(headers).build();
    }

    /**
     * GET /products : get all the products with no relationship.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of products in body
     */
    @GetMapping("/products/thin")
    public Set<ThinProductDTO> getAllThinProducts() {
        return productService.getAllThinProducts();
    }

    /**
     * GET /products : get all the products.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of products in body
     */
    @GetMapping("/products")
    public Set<ProductDTO> getAllProducts() {
        return productService.getAllProducts();
    }

    /**
     * GET /products/{id} : get single project by its id.
     *
     * @return the ResponseEntity with status 200 (OK) and the found project in body
     */
    @GetMapping("/products/{id}")
    public ResponseEntity<ProductDTO> getProduct(@NotNull @PathVariable Long id) {
        return Optional.ofNullable(productService.getProduct(id)) //
        		.map(ResponseEntity::ok) //
        		.orElse(ResponseEntity.notFound().build());
    }

    /**
     * GET /products/{id} : get single project by its id with no relationship.
     *
     * @return the ResponseEntity with status 200 (OK) and the found project in body
     */
    @GetMapping("/products/{id}/thin")
    public ResponseEntity<ThinProductDTO> getThinProduct(@NotNull @PathVariable Long id) {
    	return Optional.ofNullable(productService.getThinProduct(id)) //
        		.map(ResponseEntity::ok) //
        		.orElse(ResponseEntity.notFound().build());
    }

    /**
     * GET /products : get all the children of product by its id.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of children of the found product
     *         in body
     */
    @GetMapping("/products/{id}/children")
    public ResponseEntity<Set<ProductDTO>> getChildren(@NotNull @PathVariable Long id) {
    	return Optional.ofNullable(productService.getChildren(id)) //
        		.map(ResponseEntity::ok) //
        		.orElse(ResponseEntity.notFound().build());
    }

    /**
     * GET /products : get all the images of product by its id.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of images of the found product
     *         in body
     */
    @GetMapping("/products/{id}/images")
    public ResponseEntity<Set<ImageDTO>> getImages(@NotNull @PathVariable Long id) {
        return Optional.ofNullable(productService.getImages(id)) //
        		.map(ResponseEntity::ok) //
        		.orElse(ResponseEntity.notFound().build());
    }



}
