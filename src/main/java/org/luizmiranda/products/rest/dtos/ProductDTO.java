package org.luizmiranda.products.rest.dtos;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * A DTO for the Product entity.
 */
public class ProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String description;

    private Set<ProductDTO> children = Sets.newHashSet();

    private Set<ImageDTO> images = Sets.newHashSet();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<ProductDTO> getChildren() {
        return children;
    }

    public void setChildren(Set<ProductDTO> children) {
        this.children = children;
    }

    public Set<ImageDTO> getImages() {
        return images;
    }

    public void setImages(Set<ImageDTO> images) {
        this.images = images;
    }

	public boolean hasRecursiveChildren() {
		List<Long> childrenIds = this.getChildrenIds();
		
		return Sets.newHashSet(childrenIds).size() != childrenIds.size();
	}
	
	private List<Long> getChildrenIds() {
		
		List<Long> visitedIds = Lists.newArrayList(this.getId());
		
		for(ProductDTO child : this.getChildren()) {
			visitedIds.addAll(child.getChildrenIds());
		}
		
		return visitedIds;
		
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductDTO other = (ProductDTO) obj;
		return Objects.equal(this.id, other.id);
		
	}

	@Override
	public String toString() {
		return "ProductDTO [id=" + id + ", name=" + name + ", description=" + description + ", children=" + children
				+ ", images=" + images + "]";
	}

}
