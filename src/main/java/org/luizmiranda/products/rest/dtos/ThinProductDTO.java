package org.luizmiranda.products.rest.dtos;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Product entity.
 */
public class ThinProductDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThinProductDTO other = (ThinProductDTO) obj;
		return Objects.equals(this.id, other.getId());
	}

	@Override
	public String toString() {
		return "ThinProductDTO [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

}
