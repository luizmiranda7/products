package org.luizmiranda.products.rest.dtos;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.google.common.base.Objects;

/**
 * A DTO for the Image entity.
 */
public class ImageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    
    @NotNull
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(this.id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImageDTO other = (ImageDTO) obj;
		return Objects.equal(this.id, other.id);
	}

	@Override
	public String toString() {
		return "ImageDTO [id=" + id + ", type=" + type + "]";
	}

}
