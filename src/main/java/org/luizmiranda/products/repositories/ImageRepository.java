package org.luizmiranda.products.repositories;

import org.luizmiranda.products.domain.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {

}
