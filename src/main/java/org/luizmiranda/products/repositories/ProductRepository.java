package org.luizmiranda.products.repositories;

import org.luizmiranda.products.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
