package org.luizmiranda.products.mappers;

import java.util.List;

import org.luizmiranda.products.domain.Product;
import org.luizmiranda.products.rest.dtos.ProductDTO;
import org.luizmiranda.products.rest.dtos.ThinProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Product and its DTO ProductDTO.
 */
@Mapper(componentModel = "spring", uses = {ImageMapper.class})
public interface ProductMapper {

    ThinProductDTO productToThinProductDTO(Product Product);

    List<ThinProductDTO> productsToThinProductDTOs(List<Product> Products);

    ProductDTO productToProductDTO(Product Product);

    List<ProductDTO> productsToProductDTOs(List<Product> Products);

    @Mapping(target="images", ignore = true)
    @Mapping(target="children", ignore = true)
    Product productDTOToProduct(ProductDTO ProductDTO);

    List<Product> productDTOsToProducts(List<ProductDTO> ProductDTOs);


}

