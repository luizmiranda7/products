package org.luizmiranda.products.mappers;

import java.util.List;

import org.luizmiranda.products.domain.Image;
import org.luizmiranda.products.rest.dtos.ImageDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for the entity Image and its DTO ImageDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ImageMapper {

    ImageDTO imageToImageDTO(Image Image);

    List<ImageDTO> imagesToImageDTOs(List<Image> Images);

    @Mapping(target = "product", ignore = true)
    Image imageDTOToImage(ImageDTO ImageDTO);

    List<Image> imageDTOsToImages(List<ImageDTO> ImageDTOs);
}

