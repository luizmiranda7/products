package org.luizmiranda.products.services;


import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.luizmiranda.products.domain.Image;
import org.luizmiranda.products.domain.Product;
import org.luizmiranda.products.mappers.ImageMapper;
import org.luizmiranda.products.mappers.ProductMapper;
import org.luizmiranda.products.repositories.ImageRepository;
import org.luizmiranda.products.repositories.ProductRepository;
import org.luizmiranda.products.rest.dtos.ImageDTO;
import org.luizmiranda.products.rest.dtos.ProductDTO;
import org.luizmiranda.products.rest.dtos.ThinProductDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * Service Implementation for managing Products.
 */
@Service
@Transactional
public class ProductService {

    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ImageMapper imageMapper;

    /**
     * Save a product.
     *
     * @param productDTO the entity to save
     * @return the persisted entity
     * @throws Exception 
     */
    public ProductDTO save(ProductDTO productDTO) throws Exception {
        log.debug("Request to save Product : {}", productDTO);
        
        if(productDTO.hasRecursiveChildren())
        	throw new Exception("hasRecursiveChildren");
        	

        Optional<Product> opProduct = Optional.of(productDTO) //
        	.map(ProductDTO::getId) //
	        .map(productRepository::findOne);
        
        opProduct
        	.map(Product::getChildren) //
        	.map(Set::stream) //
        	.orElseGet(Stream::empty) //
        	.map(p -> p.parent(null)) //
        	.forEach(productRepository::save);
        
        opProduct
	        .map(Product::getImages) //
	        .ifPresent(imageRepository::delete);
        
        Product product = Optional.of(productDTO) //
	        .map(productMapper::productDTOToProduct) //
	        .map(productRepository::save).get();
        
        Set<Image> images = productDTO.getImages().stream() //
        	.map(imageMapper::imageDTOToImage) //
        	.map(i -> i.product(product)) //
        	.map(imageRepository::save) //
        	.collect(Collectors.toSet());
        product.setImages(images);
        
        Set<Product> children = productDTO.getChildren().stream() //
        	.map(ProductDTO::getId) //
        	.map(productRepository::findOne) //
        	.map(p -> p.parent(product)) //
        	.collect(Collectors.toSet());
        product.setChildren(children);
        
        productRepository.save(product);
        
        return productMapper.productToProductDTO(product);
    }

    /**
     * Delete the product by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Product : {}", id);
        productRepository.delete(id);
    }

    public Set<ThinProductDTO> getAllThinProducts() {
        return Sets
                .newHashSet(productMapper.productsToThinProductDTOs(productRepository.findAll()));
    }

    public Set<ProductDTO> getAllProducts() {
        return Sets.newHashSet(productMapper.productsToProductDTOs(productRepository.findAll()));
    }

    public ThinProductDTO getThinProduct(Long id) {
        return productMapper.productToThinProductDTO(productRepository.findOne(id));
    }

    public ProductDTO getProduct(Long id) {
        return productMapper.productToProductDTO(productRepository.findOne(id));
    }

    public Set<ProductDTO> getChildren(Long id) {
        return Optional.ofNullable(productRepository.findOne(id)) //
                .map(Product::getChildren) //
                .map(Lists::newArrayList) //
                .map(productMapper::productsToProductDTOs) //
                .map(Sets::newHashSet) //
                .orElse(Sets.newHashSet());
    }

    public Set<ImageDTO> getImages(Long id) {
        return Optional.ofNullable(productRepository.findOne(id)) //
                .map(Product::getImages) //
                .map(Lists::newArrayList) //
                .map(imageMapper::imagesToImageDTOs) //
                .map(Sets::newHashSet) //
                .orElse(Sets.newHashSet());
    }
}
