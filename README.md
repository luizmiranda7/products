# Products Management Application

To compile the application:

	mvn clean compile

To build the application generating a .jar at the target folder:

	mvn clean package

To run the application using the generated .jar file at the target folder:

	java -jar target/products-0.0.1-SNAPSHOT.jar

To run the application using the source code:

	mvn spring-boot:run

To run the tests:

	mvn clean test